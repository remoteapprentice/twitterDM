$(document).ready(function(){

// ------------------Update twitter settings-------------------------------------------------

$('#updateTwiterSettings').click(function () {
  
  var consumerKey = $('[name=consumerKey]').val()
  var consumerSecret = $('[name=consumerSecret]').val()
  var accessToken = $('[name=accessToken]').val()
  var accessTokenSecret = $('[name=accessTokenSecret]').val()
  
  if (consumerKey && consumerSecret && accessToken && accessTokenSecret) {
   
    $.post('/updateTwitterSettings', {consumerKey: consumerKey, consumerSecret: consumerSecret, accessToken: accessToken, accessTokenSecret: accessTokenSecret}, function (result, status) {
      if (status === 'success') {
        alert('updated twitter settings')
        $('[name=consumerKey]').val('')
        $('[name=consumerSecret]').val('')
        $('[name=accessToken]').val('')
        $('[name=accessTokenSecret]').val('')
      } else {
        alert('error: try again')
      }
    })
  } else {
    alert('fill all fields for twitter settings')
  }
})

// --------------------------------------END------------------------------------------------





//------------------------------------Add users to track--------------------------------------------

$('#addUsersToTrack').click(function () {
  var usersToTrack = $('[name=track]').val()
  if (usersToTrack) {
    var usersToTrackArray = usersToTrack.toString().split(',')
    $.post('/addUsersToTrack', {track: usersToTrackArray}, function (result, status) {
      if (status === 'success') {
        $('[name=track]').val('')
        getTrackedUsers()
      } else {
        alert('error: try again')
      }
    })
  } else {
    alert('input users to track \n Seperate users with commas')
  }
})

// -----------------------------------------END------------------------------------------------------





// -------------------------------------Add users to send DM-------------------------------------------
$('#addUsersToDM').click(function () {
  var usersToDM = $('[name=DM]').val()
  if (usersToDM) {
    var usersToDMArray = usersToDM.toString().split(',')
    $.post('/addUsersToDM', {DM: usersToDMArray}, function (result, status) {
      if (status === 'success') {
        $('[name=DM]').val('')
        getDMUsers()
      } else {
        alert('error: try again')
      }
    })
  } else {
    alert('input users to DM \n Seperate users with commas')
  }
})
// ------------------------------------------END-------------------------------------------------------





// -------------------------------------get all users being tracked-----------------------------------------
  function getTrackedUsers () {
    $.get('/listTracking', function (result, status) {
      $('#usersTracked').html('')
      var users = JSON.parse(result)
      for (var i in users) {
        var user = "<button class='btn btn-default tracked-users' data-btn='tracked'>" + users[i] + "&nbsp&nbsp&nbsp<i class='fa fa-trash-o' aria-hidden='true'></i></button>"
        $('#usersTracked').append(user)
      }
    })
  }
  getTrackedUsers()
// -------------------------------------------END------------------------------------------------------------




// -------------------------------------get all users to DM--------------------------------------------------
  function getDMUsers () {
    $.get('/listDM', function (result, status) {
      $('#usersToDM').html('')
      var users = JSON.parse(result)
      for (var i in users) {
        var user = "<button class='btn btn-default DM-users' data-btn='DM'>" + users[i] + "&nbsp&nbsp&nbsp<i class='fa fa-trash-o' aria-hidden='true'></i></button>"
        $('#usersToDM').append(user)
      }
    })
  }
  getDMUsers()
// -------------------------------------------END------------------------------------------------------------





// --------------------------------------Delete tracked users--------------------------------------------------

$('#usersTracked').on('click', '.btn.btn-default.tracked-users', function () {
  var user = $(this).text()
  $.post('/removeTracking', {screenName: user}, function (result, status) {
    if (status === 'success') {
      getTrackedUsers()
    } else {
      alert('error: try again')
    }
  })
})
// ---------------------------------------------END--------------------------------------------------------------


 


// --------------------------------------Delete DM users--------------------------------------------------

$('#usersToDM').on('click', '.btn.btn-default.DM-users', function () {
  var user = $(this).text()
  $.post('/removeDM', {screenName: user}, function (result, status) {
    if (status === 'success') {
      getDMUsers()
    } else {
      alert('error: try again')
    }
  })
})
// ---------------------------------------------END--------------------------------------------------------------





// --------------------------------------Start Monitor--------------------------------------------------

$(function () {
  $('#start-monitor').change(function () {
    var state = $(this).prop('checked') 
    $.post('/intervalSwitch', {iSwitch: state})
  })
})

// ---------------------------------------------END--------------------------------------------------------
})
