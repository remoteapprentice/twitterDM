var express = require('express')
var bodyParser = require('body-parser')
var app = express()


app.use(bodyParser())// get html form data
app.use(bodyParser.json()) // get JSON data
app.use(express.static('public'))


app.get('/', function (req, res) {
  res.sendfile('./index.html', {root: __dirname})
})


require('./twitterMonitor')()
app.post('/updateTwitterSettings', require('./twitterMonitor').addTwitterSettings)
app.post('/addUsersToTrack', require('./twitterMonitor').addTracking)
app.post('/removeTracking', require('./twitterMonitor').removeTracking)
app.post('/addUsersToDM', require('./twitterMonitor').addDM)
app.post('/removeDM', require('./twitterMonitor').removeDM)
app.post('/intervalSwitch', require('./twitterMonitor').intervalSwitch)
app.get('/listTracking', require('./twitterMonitor').listTracking)
app.get('/listDM', require('./twitterMonitor').listDM)

app.listen(80, '0.0.0.0', function () {
  console.log('Listening on 80')
})
